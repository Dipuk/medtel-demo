package com.dipesh.medteldemo.util;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.dipesh.medteldemo.R;

public class CommonUtils {

    static Dialog loadingDialog;


    public static void showLoading(Activity activity) {
        if (loadingDialog == null) {
            loadingDialog = new Dialog(activity);
            loadingDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            loadingDialog.setCancelable(false);
            loadingDialog.setContentView(R.layout.item_loader);
            Glide.with(activity)
                    .asGif()
                    .load(R.raw.gif_loder)
                    .into((ImageView) loadingDialog.getWindow().getDecorView().findViewById(R.id.GifImageView));
        }
        if (!activity.isFinishing())
            loadingDialog.show();
    }

    public static void hideLoading() {
        if (loadingDialog != null) {
            loadingDialog.hide();
            loadingDialog = null;
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
