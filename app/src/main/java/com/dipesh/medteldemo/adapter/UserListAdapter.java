package com.dipesh.medteldemo.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.dipesh.medteldemo.R;
import com.dipesh.medteldemo.model.UsersData;
import com.dipesh.medteldemo.ui.userinfo.UserInfoActivity;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder>{

    private final Context context;
    private final List<UsersData> usersDataList;

    public UserListAdapter(Context context, List<UsersData> usersDataList) {
        this.context = context;
        this.usersDataList = usersDataList;
    }


    @NonNull
    @NotNull
    @Override
    public UserListAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_users, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull UserListAdapter.ViewHolder holder, int position) {

        holder.mTvUserName.setText(usersDataList.get(position).getLogin());
        Glide.with(context).load(usersDataList.get(position).getAvatarUrl()).placeholder(R.drawable.ic_user).into(holder.mIvUserImage);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, UserInfoActivity.class);
                intent.putExtra("user_Name", (usersDataList.get(position).getLogin()));
                intent.putExtra("user_Image", (usersDataList.get(position).getAvatarUrl()));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return usersDataList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvUserName)
        TextView mTvUserName;
        @BindView(R.id.ivUser)
        ImageView mIvUserImage;

        public ViewHolder(@NonNull @NotNull View v) {
            super(v);
            ButterKnife.bind(this, v);


        }
    }
}
