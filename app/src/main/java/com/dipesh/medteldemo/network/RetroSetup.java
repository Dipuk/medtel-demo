package com.dipesh.medteldemo.network;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroSetup {

    private static final String Development_Url = "https://api.github.com/";

    private static Retrofit retroDemo;

    private void RetroSetup(){

    }

    public static Retrofit getRetroDemo() {

        if(retroDemo == null){
            retroDemo = new Retrofit.Builder()
                    .baseUrl(Development_Url)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        return retroDemo;
    }

}
