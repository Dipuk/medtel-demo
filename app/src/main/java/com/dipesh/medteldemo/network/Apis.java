package com.dipesh.medteldemo.network;

import com.dipesh.medteldemo.model.UsersData;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface Apis {

    @GET("users")
    Single<List<UsersData>> getUserList();
}
