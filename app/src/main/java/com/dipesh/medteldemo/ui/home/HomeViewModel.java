package com.dipesh.medteldemo.ui.home;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.dipesh.medteldemo.base.BaseAndroidViewModel;
import com.dipesh.medteldemo.model.UsersData;
import com.dipesh.medteldemo.network.Apis;
import com.dipesh.medteldemo.network.RetroSetup;

import java.net.SocketTimeoutException;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class HomeViewModel extends BaseAndroidViewModel {

    public HomeViewModel(Application application) {
        super(application);
    }

    MutableLiveData<List<UsersData>> UsersDataMutableLiveData = new MutableLiveData<>();
    MutableLiveData<Boolean> userFetchError = new MutableLiveData<>();

    public MutableLiveData<Boolean> getApplianceAddError(){
        return userFetchError;
    }

    public MutableLiveData<List<UsersData>> getUsersDataMutableLiveData(){
        return UsersDataMutableLiveData;
    }

    private MutableLiveData<Throwable> networkTimeOutError = new MutableLiveData<>();

    public LiveData<Throwable> getNetworkTimeOutError(){
        return networkTimeOutError;
    }

    public void getUserDataList(){
        Apis api = RetroSetup.getRetroDemo().create(Apis.class);

        getCompositeDisposable().add(api.getUserList().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<List<UsersData>>() {
                    @Override
                    public void onSuccess(List<UsersData> data) {
                        UsersDataMutableLiveData.setValue(data);
                        Log.d("User_data", data.toString());
                    }

                    @Override
                    public void onError(Throwable e) {
                        if(e instanceof SocketTimeoutException){
                            networkTimeOutError.setValue(e);
                        }else {
                            userFetchError.setValue(true);
                        }
                    }
                }));
    }
}
