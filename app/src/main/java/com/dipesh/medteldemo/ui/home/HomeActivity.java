package com.dipesh.medteldemo.ui.home;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.dipesh.medteldemo.R;
import com.dipesh.medteldemo.adapter.UserListAdapter;
import com.dipesh.medteldemo.model.UsersData;
import com.dipesh.medteldemo.util.CommonUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.rvUserList)
    RecyclerView mRvUserList;
    @BindView(R.id.empty_list_msg)
    TextView mTvEmptyListMsg;
    @BindView(R.id.tvOtpCode)
    TextView mTvOtpCode;
    private HomeViewModel homeViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(HomeActivity.this);
        homeViewModel = ViewModelProviders.of(HomeActivity.this).get(HomeViewModel.class);

        if (this.getIntent().hasExtra("OTP")) {

            mTvOtpCode.setText(getIntent().getStringExtra("OTP"));
        }

        if(CommonUtils.isNetworkAvailable(HomeActivity.this)) {
            CommonUtils.showLoading(HomeActivity.this);
            homeViewModel.getUserDataList();
        }
        else {
            Toast.makeText(HomeActivity.this, "Internet not found", Toast.LENGTH_LONG).show();

        }

        observableViewModel();

    }

    public void observableViewModel(){

        homeViewModel.getUsersDataMutableLiveData().observe(this, new Observer<List<UsersData>>() {
            @Override
            public void onChanged(List<UsersData> data) {
                CommonUtils.hideLoading();
                if(data != null){
//                    if(data.size() == 1 || data.size() == 0){
                    if(data.size() == 0){
                        mTvEmptyListMsg.setVisibility(View.VISIBLE);
                        mRvUserList.setVisibility(View.GONE);
                    }
                    else {
                        mTvEmptyListMsg.setVisibility(View.GONE);
                        mRvUserList.setVisibility(View.VISIBLE);
                        setUpRecyclerView(data);

                    }

                }
                else {
                    Toast.makeText(HomeActivity.this, "Data not found", Toast.LENGTH_LONG).show();
                }
            }
        });

        homeViewModel.getApplianceAddError().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                CommonUtils.hideLoading();
                Toast.makeText(HomeActivity.this, "APi loading failed", Toast.LENGTH_SHORT).show();

            }
        });


        homeViewModel.getNetworkTimeOutError().observe(this, new Observer<Throwable>() {
            @Override
            public void onChanged(Throwable throwable) {
                CommonUtils.hideLoading();
                Toast.makeText(HomeActivity.this, "Network Error occurred", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void setUpRecyclerView(List<UsersData> data){

        UserListAdapter userListAdapter = new UserListAdapter(HomeActivity.this, data);
        mRvUserList.setAdapter(userListAdapter);
    }

}