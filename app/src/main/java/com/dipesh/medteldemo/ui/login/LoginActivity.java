package com.dipesh.medteldemo.ui.login;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.dipesh.medteldemo.R;
import com.dipesh.medteldemo.ui.home.HomeActivity;
import com.dipesh.medteldemo.util.CommonUtils;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity {

    private String verificationCodeBySystem;
    private final int mResendCodeCount= 0;

    @BindView(R.id.login_layout)
    RelativeLayout loginLayout;
    @BindView(R.id.otp_layout)
    RelativeLayout otpLayout;

    @BindView(R.id.etMobileNumber)
    EditText mMobileNumber;
    @BindView(R.id.btSendOtp)
    Button sendOTP;
    @BindView(R.id.etOtpNumber)
    EditText mOtpNumber;
    @BindView(R.id.btSubmitOtp)
    Button submitOtp;
    @BindView(R.id.tvResendOtp)
    TextView resendOtp;
    @BindView(R.id.tvResendOtpTimerText)
    TextView mResendOtpTimerText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btSendOtp)
    public void verifyMobileNumber() {
        callVerificationMethod(mMobileNumber.getText().toString());

    }

    @OnClick(R.id.btSubmitOtp)
    public void submitOtpMethod(){
        String mOtpValue = mOtpNumber.getText().toString();
        submitOtp.setVisibility(View.GONE);
        if(CommonUtils.isNetworkAvailable(LoginActivity.this)) {
            if (checkValidOtp(mOtpValue)) {
                verifyOtp(mOtpValue);
            } else {
                Toast.makeText(LoginActivity.this, "Please enter valid OTP", Toast.LENGTH_LONG).show();
            }
        }
    }

    @OnClick(R.id.tvResendOtp)
    public void resendOtp(){
        Toast.makeText(LoginActivity.this, "Code resending", Toast.LENGTH_SHORT).show();
        callVerificationMethod(mMobileNumber.getText().toString());
//        resendOtp.setEnabled(false);
//        resendOtp.setTextColor(ContextCompat.getColor(LoginActivity.this,R.color.grey));

    }

    private void callVerificationMethod(String mobileNumber){
        if (CommonUtils.isNetworkAvailable(LoginActivity.this)) {
            if (checkValidMobileNumber(mobileNumber)) {
                sendVerificationCodeToUser(mobileNumber);

            } else {
                showMobileNumberError();
            }
        }
        else {
            Toast.makeText(LoginActivity.this, "Internet not found", Toast.LENGTH_LONG).show();
        }
    }

    public boolean checkValidOtp(String otp){
        return otp.length() == 6;
    }

    public boolean checkValidMobileNumber(String mobile){
        return mobile.length() == 10;
    }

    public void showMobileNumberError() {
        mMobileNumber.setError("Enter a valid mobile number");
    }

    public void showOtpNumberError() {
        mOtpNumber.setError("Enter a valid OTP number");
    }

    private final PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new
            PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                @Override
                public void onCodeSent(@NonNull String s, @NonNull PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                    super.onCodeSent(s, forceResendingToken);
                    CommonUtils.hideLoading();
                    verificationCodeBySystem = s;
                    showOtpLayout();

                }

                @Override
                public void onVerificationCompleted(@NonNull PhoneAuthCredential phoneAuthCredential) {
                    CommonUtils.hideLoading();
                    signInTheUserByCredential(phoneAuthCredential);

                }

                @Override
                public void onVerificationFailed(@NonNull FirebaseException e) {
                    CommonUtils.hideLoading();
                    Toast.makeText(LoginActivity.this,e.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.d("Firebase_Failed", String.valueOf(e.getMessage()));
                }

                @Override
                public void onCodeAutoRetrievalTimeOut(@NonNull String s) {
                    super.onCodeAutoRetrievalTimeOut(s);
                    CommonUtils.hideLoading();
                    Toast.makeText(LoginActivity.this,s, Toast.LENGTH_SHORT).show();
                    Log.d("Firebase_Failed_TimeOut", s);
                }
            };

    private void verifyOtp(String codeByuser) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationCodeBySystem, codeByuser);
        signInTheUserByCredential(credential);

    }


    private void signInTheUserByCredential(PhoneAuthCredential credential) {
        String smsCode = credential.getSmsCode();
        final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

        firebaseAuth.signInWithCredential(credential).addOnCompleteListener(LoginActivity.this, task -> {
            if (task.isSuccessful()) {
//                FirebaseUser user = task.getResult().getUser();
                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                intent.putExtra("OTP", smsCode);
                startActivity(intent);
                finish();
            } else {
                // show Otp Validation Error
                Toast.makeText(LoginActivity.this,task.getException().getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }

    private void sendVerificationCodeToUser(String phoneNumber) {
        CommonUtils.showLoading(LoginActivity.this);
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+91" + phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                LoginActivity.this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
    }

    private void manageOtp(){
        CountDownTimer tick = new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                long timeLeft = millisUntilFinished / 1000;
                String mTimerText = " Code expires in " + timeLeft + " sec";
                mResendOtpTimerText.setText(mTimerText);
                resendOtp.setEnabled(false);
            }

            public void onFinish() {

                if(mResendCodeCount < 4){
                    String mTimerText = " Code expired";
                    mResendOtpTimerText.setText(mTimerText);
                    resendOtp.setEnabled(true);
                    resendOtp.setTextColor(ContextCompat.getColor(LoginActivity.this, R.color.design_default_color_primary));

                    submitOtp.setVisibility(View.VISIBLE);

                }
                else {
                    Toast.makeText(LoginActivity.this, "Please try after some time", Toast.LENGTH_SHORT).show();

                }
            }

        }.start();
    }



    private void showOtpLayout(){
        loginLayout.setVisibility(View.GONE);
        otpLayout.setVisibility(View.VISIBLE);
        manageOtp();
        resendOtp.setTextColor(ContextCompat.getColor(LoginActivity.this,R.color.grey));

    }


}