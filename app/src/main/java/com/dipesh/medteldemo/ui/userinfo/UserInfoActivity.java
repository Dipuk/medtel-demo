package com.dipesh.medteldemo.ui.userinfo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dipesh.medteldemo.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UserInfoActivity extends AppCompatActivity {

    @BindView(R.id.ivUser)
    ImageView mIvUser;
    @BindView(R.id.tvUserName)
    TextView mTvUserName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uset_info);
        ButterKnife.bind(this);

        if (this.getIntent().hasExtra("user_Name")) {

            mTvUserName.setText(String.format("Hi my name is%s", getIntent().getStringExtra("user_Name")));
            Glide.with(this).load(getIntent().getStringExtra("user_Image")).placeholder(R.drawable.ic_user).into(mIvUser);

        }

    }

    public void onClickBack(View view) {
        finish();
    }
}