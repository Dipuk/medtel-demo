package com.dipesh.medteldemo.base;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;

import io.reactivex.disposables.CompositeDisposable;

public class BaseAndroidViewModel extends AndroidViewModel {
    private CompositeDisposable mCompositeDisposable;

    public BaseAndroidViewModel(Application application){
        super(application);
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    protected void onCleared() {
        mCompositeDisposable.dispose();
        super.onCleared();
    }

    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }
}
